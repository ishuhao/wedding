import Taro from '@tarojs/taro'
import React from 'react'
import { View } from '@tarojs/components'
import { useStatusBar } from '@/hooks'
import NavigatorBarStyle from './NavigatorBar.module.scss'

function NavigatorBar(props: { title?: string, children?: any }) {
  // statusBar的高度
  const barStyle = useStatusBar()
  /**
   * 返回到上个页面
   */
  const handleBackToPrevPage = () => {
    // 获取页面数量
    const pageSize = Taro.getCurrentPages().length
    // 如果页面堆栈中的数量大于1，则返回到堆栈中
    if (pageSize > 1) {
      Taro.navigateBack()
      return
    }
    // 如果只有一页，则打开首页
    Taro.redirectTo({ url: '/pages/index/index' })
  }
  return (
    <View 
      className={NavigatorBarStyle['bar-container']} 
      style={{ 
        paddingTop: `${barStyle.statusBar}PX`,
        height: `${barStyle.statusBar + barStyle.menuButton.wrap}PX`
      }}
    >
      <View 
        className={NavigatorBarStyle['arrow']}
        style={{ height: `${barStyle.menuButton.wrap}PX` }}
        onClick={_ => handleBackToPrevPage()}
      />
      <View 
        className={NavigatorBarStyle['content']}
        style={{ height: `${barStyle.menuButton.wrap}PX` }}
      >{props.title ? props.title : props.children}</View>
    </View>
  )
}

export default React.memo(NavigatorBar);