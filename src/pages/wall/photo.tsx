import Taro, { usePageScroll, useShareAppMessage } from '@tarojs/taro'
import React, { useState, ReactElement } from 'react'
import { View, Swiper, Image, SwiperItem, ScrollView } from '@tarojs/components'
import { useStatusBar } from '@/hooks'
import NavigatorBar from './components/NavigatorBar'
import PhotoStyle from './photo.module.scss'

// 精修照片
const PHOTOS_WALL = [
  '000-2.jpg',
  '000-4.jpg',
  '000-8.jpg',
  '010-1.jpg',
  '011-1.jpg',
  '015.jpg',
  '016-1.jpg',
  '036-1.jpg',
  '038-1.jpg',
  '039.jpg',
  '042.jpg',
  '048-1.jpg',
  '060.jpg',
  '061.jpg',
  '066-1.jpg',
  '069.jpg',
  '075.jpg',
  '080.jpg',
  '086-1.jpg',
  '087.jpg',
  '089.jpg',
  '099.jpg',
  '100.jpg',
  '101.jpg',
  '103.jpg',
  '115.jpg',
  '116.jpg',
  '119.jpg',
  '123.jpg',
  '128.jpg',
  '130.jpg',
  '139.jpg',
  '143.jpg',
  '144.jpg',
  '154.jpg',
  '160.jpg',
  '161.jpg',
  '167.jpg',
  '176.jpg',
  '181.jpg',
  '184.jpg',
  '185.jpg',
  '187.jpg',
  '191.jpg',
  '192-1.jpg',
  '194.jpg',
  '195.jpg',
  '196.jpg',
  '199.jpg',
  '200.jpg',
  '202.jpg',
  '205.jpg',
  '206.jpg',
  '216.jpg',
  '220.jpg'
].map(url => `https://shuhao-1301686320.cos.ap-nanjing.myqcloud.com/${url}`)

// 底片
const ORIGIN_PHOTO_WALL = [
  '000-1.jpg',
  '000-3.jpg',
  '000-5.jpg',
  '000-6.jpg',
  '000-7.jpg',
  '001.jpg',
  '002.jpg',
  '003.jpg',
  '004.jpg',
  '005.jpg',
  '006.jpg',
  '007.jpg',
  '008.jpg',
  '009.jpg',
  '012.jpg',
  '013.jpg',
  '014.jpg',
  '017.jpg',
  '018.jpg',
  '019.jpg',
  '020.jpg',
  '021.jpg',
  '022.jpg',
  '023.jpg',
  '024.jpg',
  '025.jpg',
  '026.jpg',
  '027.jpg',
  '028.jpg',
  '029.jpg',
  '030.jpg',
  '031.jpg',
  '032.jpg',
  '033.jpg',
  '034.jpg',
  '035.jpg',
  '037.jpg',
  '040.jpg',
  '041.jpg',
  '043.jpg',
  '044.jpg',
  '045.jpg',
  '046.jpg',
  '047.jpg',
  '049.jpg',
  '050.jpg',
  '051.jpg',
  '052.jpg',
  '053.jpg',
  '054.jpg',
  '055.jpg',
  '056.jpg',
  '057.jpg',
  '058.jpg',
  '059.jpg',
  '062.jpg',
  '063.jpg',
  '064.jpg',
  '065.jpg',
  '067.jpg',
  '068.jpg',
  '070.jpg',
  '071.jpg',
  '072.jpg',
  '073.jpg',
  '074.jpg',
  '076.jpg',
  '077.jpg',
  '078.jpg',
  '079.jpg',
  '081.jpg',
  '082.jpg',
  '083.jpg',
  '084.jpg',
  '085.jpg',
  '088.jpg',
  '090.jpg',
  '091.jpg',
  '092.jpg',
  '093.jpg',
  '094.jpg',
  '095.jpg',
  '096.jpg',
  '097.jpg',
  '098.jpg',
  '102.jpg',
  '104.jpg',
  '105.jpg',
  '106.jpg',
  '107.jpg',
  '108.jpg',
  '109.jpg',
  '110.jpg',
  '111.jpg',
  '112.jpg',
  '113.jpg',
  '114.jpg',
  '117.jpg',
  '118.jpg',
  '120.jpg'
].map(url => `https://shuhao-negative-1301686320.cos.ap-nanjing.myqcloud.com/compress/${url}`)

const circular = true
const lazyLoad = true
const scrollY = true

enum PHOTO_TYPE {
  // 精修
  RETOUCHING = 0,
  // 原始
  ORIGIN = 1
}

enum SHOW_TYPE {
  // 轮播
  SWIPER = 0,
  // 瀑布流
  WATERFALL = 1
}

function PhotoWall(): ReactElement {
  const [ photoType, setPhotoType ] = useState(PHOTO_TYPE.RETOUCHING)

  useShareAppMessage(() => ({
    title: '婚礼相册',
    path: '/pages/wall/photo'
  }))

  /**
   * 切换精修或者底图
   * @param type 展示类型
   */
  const changePhotoType = type => {
    // 设置照片类型
    setPhotoType(type)
    // 页面滚动到顶部
    Taro.pageScrollTo({ scrollTop: 0, duration: 0 })
  }

  return (
    <>
      <NavigatorBar>
        <View className={PhotoStyle['switch-wrap']}>
          <View 
            className={[PhotoStyle['switch'], photoType == PHOTO_TYPE.RETOUCHING ? PhotoStyle['active'] : ''].join(' ')}
            onClick={_ => changePhotoType(0)}
          >
            精修图
          </View>
          <View 
            className={[PhotoStyle['switch'], photoType == PHOTO_TYPE.ORIGIN ? PhotoStyle['active'] : ''].join(' ')}
            onClick={_ => changePhotoType(1)}
          >
            底片
          </View>
        </View>
      </NavigatorBar>
      {/* 精修图 */}
      <PageContainer type={PHOTO_TYPE.RETOUCHING} current={photoType} />
      {/* 底片 */}
      <PageContainer type={PHOTO_TYPE.ORIGIN} current={photoType} />
    </>
  );
}

function handleShowOriginImage(imgUrl) {
  Taro.previewImage({ urls: [ imgUrl ] })
}

const defaultProps = {
  showType: 0,
  onChange: (type: number) => {}
}

type IProps = typeof defaultProps

/**
 * 按钮组
 */
const BtnWrap = React.memo((props: IProps = defaultProps) => {
  const barStyle = useStatusBar()
  return (
    <View 
      className={[
        PhotoStyle['btn-wrap']
      ].join(' ')} 
      style={{ top: `${barStyle.total + 20}PX` }}
    >
      <View 
        className={[PhotoStyle[ props.showType == SHOW_TYPE.SWIPER ? 'btn-danger' : 'btn-primary' ], PhotoStyle['btn']].join(' ')}
        onClick={_ => props.onChange(0)}
      >
        轮播
      </View>
      <View 
        className={[PhotoStyle[ props.showType == SHOW_TYPE.WATERFALL ? 'btn-danger' : 'btn-primary' ], PhotoStyle['btn']].join(' ')}
        onClick={_ => props.onChange(1)}
      >
        瀑布流
      </View>
    </View>
  )
})

/**
 * 轮播方式展示照片
 */
const PhotoSwiper = React.memo((props: { photos: string[], show: boolean }) => {
  return (
    <Swiper
      autoplay={props.show}
      circular={circular}
      previousMargin='80rpx'
      nextMargin='80rpx'
      className={[
        PhotoStyle['photo-swiper'],
        'animated linear',
        props.show ? 'move-entry' : 'move-leave'
      ].join(' ')}
    >
      {
        props.photos.map(imgUrl => (
          <SwiperItem
            className={PhotoStyle['photo-item']}
            key={`image_${imgUrl}`}
          >
            <View className={PhotoStyle['photo-wrap']}>
              <Image 
                onClick={_ => handleShowOriginImage(`${imgUrl}`)} 
                className={PhotoStyle['photo']} 
                mode='widthFix' 
                src={`${imgUrl}?imageView2/2/w/550/q/70`}
                lazyLoad={lazyLoad}
              />
            </View>
          </SwiperItem>
        ))
      }
    </Swiper>
  )
})

/**
 * 瀑布流方式展示照片
 */
const PhotoScroll = React.memo((props: { photos: string[], show: boolean, isOrigin: boolean }) => {
  const barStyle = useStatusBar()
  // 左右展示图片的中位线
  const middle = Math.floor(props.photos.length / 2) - (props.isOrigin ? 2 : 0)
  return (
    <ScrollView
      scrollY={scrollY}
      className={[
        PhotoStyle['full-screen'],
        'animated linear',
        props.show ? 'move-entry-reverse' : 'move-leave-reverse'
      ].join(' ')}
    >
      <View 
        className={PhotoStyle['photo-wall']}
        style={{ marginTop: `${barStyle.total}PX` }}
      >
        <View className={PhotoStyle['row']}>
          {
            props.photos.slice(0, middle).map(imgUrl => (
              <View 
                className={PhotoStyle['photo-wrap']} 
                key={`image_${imgUrl}`}
              >
                <Image
                  onClick={_ => handleShowOriginImage(`${imgUrl}`)}
                  className={PhotoStyle['photo']}
                  mode='widthFix'
                  src={`${imgUrl}?imageView2/2/w/550/q/70`}
                  lazyLoad={lazyLoad}
                />
              </View>
            ))
          }
        </View>
        <View className={PhotoStyle['row']}>
          {
            props.photos.slice(middle).map(imgUrl => (
              <View 
                className={PhotoStyle['photo-wrap']} 
                key={`image_${imgUrl}`}
              >
                <Image
                  onClick={_ => handleShowOriginImage(`${imgUrl}`)}
                  className={PhotoStyle['photo']}
                  mode='widthFix'
                  src={`${imgUrl}?imageView2/2/w/550/q/70`}
                  lazyLoad={lazyLoad}
                />
              </View>
            ))
          }
        </View>
      </View>
    </ScrollView>
  )
})

const PageContainer = React.memo((props: { type: number, current: number }) => {
  const [ showType, setShowType ] = useState(SHOW_TYPE.SWIPER)
  return (
    <View 
      className={[
        PhotoStyle['page'],
        props.type == PHOTO_TYPE.ORIGIN ? PhotoStyle['rotate'] : PhotoStyle['rotate-reverse'],
        props.type === props.current ? '' : PhotoStyle['leave']
      ].join(' ')}
    >
      <BtnWrap 
        showType={showType} 
        onChange={type => setShowType(type)}
      />
      {/* 轮播组件 */}
      <PhotoSwiper 
        photos={props.type == PHOTO_TYPE.ORIGIN ? ORIGIN_PHOTO_WALL : PHOTOS_WALL} 
        show={showType == SHOW_TYPE.SWIPER} 
      />
      {/* 瀑布流 */}
      <PhotoScroll 
        isOrigin={props.type == PHOTO_TYPE.ORIGIN}
        photos={props.type == PHOTO_TYPE.ORIGIN ? ORIGIN_PHOTO_WALL : PHOTOS_WALL}
        show={showType == SHOW_TYPE.WATERFALL}
      />
    </View>
  )
})

export default React.memo(PhotoWall);
