import { combineReducers } from 'redux'
import counter, { ICount } from './counter'
import home, { IHome } from './home'

const reducers = {
  counter,
  home
}

export type IRootReducers = {
  count: ICount
  home: IHome
}

export default combineReducers(reducers)
