import { useState, useEffect } from 'react'
import Taro from '@tarojs/taro'

// 安全头部
let navigatorBar = {
  statusBar: 20,
  menuButton: {
    wrap: 40,
    height: 32,
  },
  total: 60
}

export function useStatusBar() {
  const [ barStyle, setBarStyle ] = useState(navigatorBar)
  // 接口请求
  useEffect(() => {
    (async () => {
      // 状态栏的高度
      const { statusBarHeight } = Taro.getSystemInfoSync()
      const result = await Taro.getMenuButtonBoundingClientRect()
      setBarStyle({
        statusBar: statusBarHeight,
        menuButton: {
          wrap: (result.top - statusBarHeight) * 2 + result.height,
          height: result.height
        },
        total: statusBarHeight + (result.top - statusBarHeight) * 2 + result.height
      })
    })()
  }, [])
  return barStyle
}