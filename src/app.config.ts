export default {
  pages: [
    'pages/index/index',
    'pages/wall/photo'
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'wedding',
    navigationBarTextStyle: 'black'
  }
}
