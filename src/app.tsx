import { Provider } from 'react-redux'

import configStore from './store'

import './app.scss'

const store = configStore()

function App(props) {
  return (
    <Provider store={store}>
      {props.children}
    </Provider>
  )
}

export default App
