import Taro, { useShareAppMessage } from '@tarojs/taro'
import { ReactElement, useState, useEffect } from 'react'
import { View } from '@tarojs/components'
import Template1 from './components/Template1'
import indexStyle from './index.module.scss'

// 音频对象
const audio = Taro.createInnerAudioContext()
// 音频链接
audio.src = 'https://music-1301686320.cos.ap-nanjing.myqcloud.com/sugar.mp3'

function Index(): ReactElement {
  useShareAppMessage(() => ({
    title: '束 & 柳的婚礼',
    path: '/pages/index/index'
  }))

  const [ playing, setPlaying ] = useState(true) 

  useEffect(() => {
    // 如果当前需要播放
    if (playing) {
      // 开始播放
      audio.play()
      // 音频播放结束，则继续重头开始播放
      audio.onEnded(() => {
        audio.play()
      })
      return
    }
    audio.pause()
  }, [ playing ])

  return (
    <View className={indexStyle.index}>
      <View className={[indexStyle['icon-music'], 'animated infinite rotate duration-longest linear', playing ? '' : 'pause'].join(' ')} onClick={_ => { setPlaying(!playing) }} />
      <Template1 />
    </View>
  )
}

export default Index

