import { UPDATE_AVATAR } from '../constants/home'

const INITIAL_STATE = {
  groom: {
    name: '束豪',
    phone: '18066119608',
    avatar: 'https://shuhao-1301686320.cos.ap-nanjing.myqcloud.com/069.jpg?imageView2/2/w/500/q/80'
  },
  bride: {
    name: '柳静静',
    phone: '18951935936',
    avatar: 'https://shuhao-1301686320.cos.ap-nanjing.myqcloud.com/066-1.jpg?imageView2/2/w/500/q/80'
  },
  position: {
    detail: '六安市舒城县城关镇盛世人和酒店2楼',
    latitude: 31.477724,
    longitude: 116.941828
  },
  timeDate: '2021.09.12 星期日',
  pages: [
    {
      avatar: 'https://shuhao-1301686320.cos.ap-nanjing.myqcloud.com/119.jpg?imageMogr2/scrop/400x400',
      time: '2021/09/12',
      name: '束先生 & 柳小姐',
    },
    {
      avatar: 'https://shuhao-1301686320.cos.ap-nanjing.myqcloud.com/101.jpg?imageView2/2/w/700/q/80',
      data: [
        { style: 'tip', title: '在这最美丽的季节里' },
        { style: 'tip', title: '我们相遇，相爱' },
        { style: 'tip', title: '我们的幸福需要你的见证' },
        { style: 'title', title: '【时间 · Time】' },
        { style: 'tip', title: '2021.09.12 星期天' },
        { style: 'title', title: '【地点 · Address】' },
        { style: 'tip', title: '安徽省舒城县盛世人和酒店2楼' }
      ]
    },
    {
      words: [
        '如果我看过你看过的世界，',
        '走过你走过的路，',
        '是不是就能更靠近你一点。'
      ]
    },
    {
      words: [
        '梦见了，才知这份深情。',
        '无力抗拒，我不求天长地久的美丽，',
        '只求生生世世的轮回里有你有我。'
      ]
    },
    {
      avatars: [
        'https://shuhao-1301686320.cos.ap-nanjing.myqcloud.com/154.jpg?imageView2/2/w/700/q/80',
        'https://shuhao-1301686320.cos.ap-nanjing.myqcloud.com/199.jpg?imageView2/2/w/700/q/80'
      ],
      words: [
        '我想在你想结婚我也想结婚，',
        '你爱我我也爱你的年纪遇见你。'
      ]
    },
    {
      avatars: [
        'https://shuhao-1301686320.cos.ap-nanjing.myqcloud.com/160.jpg?imageView2/2/w/500/q/80',
        'https://shuhao-1301686320.cos.ap-nanjing.myqcloud.com/176.jpg?imageView2/2/w/500/q/80',
        'https://shuhao-1301686320.cos.ap-nanjing.myqcloud.com/154.jpg?imageView2/2/w/500/q/80'
      ],
      words: [
        '世界上最幸福的事',
        '大概就是和心中挚爱一起长相厮守吧！',
        '感谢命运的安排，有你我的人生才是完整',
        '从今以后你我白首不离'
      ]
    },
    {
      avatar: 'https://shuhao-1301686320.cos.ap-nanjing.myqcloud.com/128.jpg?imageView2/2/w/500/q/80',
      circle: 'https://shuhao-1301686320.cos.ap-nanjing.myqcloud.com/220.jpg?imageMogr2/crop/500x500',
      data: [
        { style: 'sub-title', title: 'AM 08:00-10:00' },
        { style: 'content', title: '迎接新娘' },
        { style: 'sub-title', title: 'AM 10:00-11:30' },
        { style: 'content', title: '宾客签到' },
        { style: 'sub-title', title: 'AM 11:30-12:00' },
        { style: 'content', title: '婚礼仪式' },
        { style: 'sub-title', title: 'PM 12:00-14:00' },
        { style: 'content', title: '婚宴开始' }
      ]
    }
  ]

}

export type IHome = typeof INITIAL_STATE

export default function home(state: IHome = INITIAL_STATE, action) {
  switch (action.type) {
    case UPDATE_AVATAR:
      return state
    default:
      return state
  }
}